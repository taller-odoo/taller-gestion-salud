# Carpetas básicas para la App

Dentro de la carpeta *gestion*:

- Crear la carpeta `models`
- Crear la carpeta `views`
- Crear la carpeta `security`

Modificar el contenido de `__init__.py`:

```python
from . import models
```
Dentro de la carpeta *gestion/models*:

- Crear la archivo `__init__.py`
- Crear la archivo `gestion.py`

Contenido de `models/__init__.py`

```python
from . import gestion
```

# Creación del modelo Paciente

En el archivo `models/gestion.py`


```python
# -*- coding: utf-8 -*-

from odoo import fields, models


class Paciente(models.Model):
    _name = 'gestion.paciente'
    _description = u'Paciente'

    _inherits = {'res.partner': 'partner_id'}

    dni = fields.Char(
        string='DNI',
        required=True,
        size=8,
    )

    sexo = fields.Selection(
        selection=[
            ('M', 'Masculino'),
            ('F', 'Femenino'),
        ],
        required=True,
    )

    _sql_constraints = [
        ('dni_uniq', 'unique (dni)', 'El DNI debe ser único'),
    ]

```


# Vistas para el modelo Paciente

En el archivo `views/gestion_views.xml`

```xml
<?xml version="1.0" encoding="utf-8"?>
<odoo>
    <data>

        <!-- VISTAS DEL MODELO PACIENTE -->
        
        <!-- VISTA SEARCH -->
        <record id="paciente_search_view" model="ir.ui.view">
            <field name="name">gestion.paciente.search</field>
            <field name="model">gestion.paciente</field>
            <field name="arch" type="xml">
                <search>
                    <field name="dni" string="DNI"/>
                    <field name="name" string="Apellidos y Nombres"/>
                </search>
            </field>
        </record>

        <!-- VISTA TREE -->
        <record model="ir.ui.view" id="paciente_tree_view">
            <field name="name">gestion.paciente.tree</field>
            <field name="model">gestion.paciente</field>
            <field name="arch" type="xml">
                <tree>
                    <field name="dni"/>
                    <field name="name"/>
                    <field name="sexo"/>
                    <field name="mobile"/>
                </tree>
            </field>
        </record>

        <!-- VISTA FORMULARIO -->
        <record model="ir.ui.view" id="paciente_form_view">
            <field name="name">gestion.paciente.form</field>
            <field name="model">gestion.paciente</field>
            <field name="arch" type="xml">
                <form>
                    <sheet>
                        <group string="Datos del paciente">
                            <group colspan="4">
                                <field name="name"/>
                                <field name="sexo"/>
                                <field name="dni"/>
                            </group>
                        </group>
                        <group string="Domicilio">
                            <field name="street"/>
                            <field name="mobile"/>
                        </group>
                    </sheet>
                </form>
            </field>
        </record>


        <!-- ACTION VIEW -->
        <record id="paciente_action" model="ir.actions.act_window">
            <field name="name">Pacientes</field>
            <field name="type">ir.actions.act_window</field>
            <field name="res_model">gestion.paciente</field>
            <field name="view_mode">tree,form</field>
            <field name="view_type">form</field>
            <field name="limit">25</field>
            <field name="domain">[]</field>
            <field name="context">{}</field>
        </record>

        <!-- MENU DE LA APLICACION -->
        <menuitem
            id="menu_main"
            name="Gestión Hospitalaria" sequence="3"/>

        <!-- MENU PACIENTES -->
        <menuitem id="menu_pacientes"
            name="Pacientes" 
            parent="menu_main"
            action="paciente_action" 
            sequence="1" />

    </data>
</odoo>
```

En el archivo `__manifest__.py`

Dentro de la sección:

```python
    'data': [

    ],
```

Agregar:

```python
        'views/gestion_views.xml',
```

Quedando como:

```python
    'data': [
        'views/gestion_views.xml',
    ],
```


[Anterior](./parte-1.md) [Siguiente](./parte-3.md)
