# Creación del modelo Médico

En el archivo `models/gestion.py`


```python


class Medico(models.Model):
    _name = 'gestion.medico'
    _description = u'Médico'

    _inherits = {'hr.employee': 'employee_id'}

    dni = fields.Char(
        string='DNI',
        required=True,
        size=8
    )

    sexo = fields.Selection(
        selection=[
            ('M', 'Masculino'),
            ('F', 'Femenino'),
        ],
        required=True,
    )

    _sql_constraints = [
        ('dni_uniq', 'unique (dni)', 'El DNI debe ser único'),
    ]


```


# Vistas para el modelo Médico

En el archivo `views/gestion_views.xml`

```xml
        <!-- VISTAS DEL MODELO MEDICO -->
        
        <!-- VISTA SEARCH -->
        <record id="medico_search_view" model="ir.ui.view">
            <field name="name">gestion.medico.search</field>
            <field name="model">gestion.medico</field>
            <field name="arch" type="xml">
                <search>
                    <field name="dni" string="DNI"/>
                    <field name="name" string="Apellidos y Nombres"/>
                </search>
            </field>
        </record>

        <!-- VISTA TREE -->
        <record model="ir.ui.view" id="medico_tree_view">
            <field name="name">gestion.medico.tree</field>
            <field name="model">gestion.medico</field>
            <field name="arch" type="xml">
                <tree>
                    <field name="dni"/>
                    <field name="name"/>
                    <field name="sexo"/>
                </tree>
            </field>
        </record>

        <!-- VISTA FORMULARIO -->
        <record model="ir.ui.view" id="medico_form_view">
            <field name="name">gestion.medico.form</field>
            <field name="model">gestion.medico</field>
            <field name="arch" type="xml">
                <form>
                    <sheet>
                        <group string="Datos del médico">
                            <group colspan="4">
                                <field name="name"/>
                                <field name="sexo"/>
                                <field name="dni"/>
                            </group>
                        </group>
                    </sheet>
                </form>
            </field>
        </record>

        <!-- ACTION VIEW -->
        <record id="medico_action" model="ir.actions.act_window">
            <field name="name">medicos</field>
            <field name="type">ir.actions.act_window</field>
            <field name="res_model">gestion.medico</field>
            <field name="view_mode">tree,form</field>
            <field name="view_type">form</field>
            <field name="limit">25</field>
            <field name="domain">[]</field>
            <field name="context">{}</field>
        </record>

        <!-- MENU MEDICOS -->
        <menuitem id="menu_medicos"
            name="Médicos" 
            parent="menu_main"
            action="medico_action" 
            sequence="2" />

```


# Creación del modelo Especialidad del Médico

En el archivo `models/gestion.py`


```python


class Especialidad(models.Model):
    _name = 'gestion.especialidad'
    _description = u'Especialidad Médica'

    name = fields.Char('Especialidad')

    _sql_constraints = [
        ('name_uniq', 'unique (name)', 'La especialidad debe ser única'),
    ]



```


# Vistas especialidades del Médico

En el archivo `views/gestion_views.xml`

```xml
        <!-- VISTAS DEL MODELO ESPECIALIDAD -->
        
        <!-- VISTA SEARCH -->
        <record id="especialidad_search_view" model="ir.ui.view">
            <field name="name">gestion.especialidad.search</field>
            <field name="model">gestion.especialidad</field>
            <field name="arch" type="xml">
                <search>
                    <field name="name"/>
                </search>
            </field>
        </record>

        <!-- VISTA TREE -->
        <record model="ir.ui.view" id="especialidad_tree_view">
            <field name="name">gestion.especialidad.tree</field>
            <field name="model">gestion.especialidad</field>
            <field name="arch" type="xml">
                <tree>
                    <field name="name"/>
                </tree>
            </field>
        </record>

        <!-- VISTA FORMULARIO -->
        <record model="ir.ui.view" id="especialidad_form_view">
            <field name="name">gestion.especialidad.form</field>
            <field name="model">gestion.especialidad</field>
            <field name="arch" type="xml">
                <form>
                    <sheet>
                        <group colspan="4">
                            <field name="name"/>
                        </group>
                    </sheet>
                </form>
            </field>
        </record>

        <!-- ACTION VIEW -->
        <record id="especialidad_action" model="ir.actions.act_window">
            <field name="name">Especialidades</field>
            <field name="type">ir.actions.act_window</field>
            <field name="res_model">gestion.especialidad</field>
            <field name="view_mode">tree,form</field>
            <field name="view_type">form</field>
            <field name="limit">25</field>
            <field name="domain">[]</field>
            <field name="context">{}</field>
        </record>




        <!-- MENU ESPECIALIDADES -->
        <menuitem id="menu_especialidades"
            name="Especialidades" 
            parent="menu_main"
            action="especialidad_action" 
            sequence="3" />


```


[Anterior](./parte-2.md) [Siguiente](./parte-4.md)
