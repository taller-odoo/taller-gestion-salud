Taller Odoo - Minsa
===================

Ruta:

`/home/$USER/apps-taller`


# Creación de Módulo

## Archivos básicos

- Crear la carpeta `gestion`
- Crear el archivo `gestion/__init__.py`
- Crear la archivo `gestion/__manifest__.py`

```bash
cd
mkdir -p apps-taller/gestion
cd apps-taller/gestion
touch __init__.py
touch __manifest__.py
```

### Contenido básico de los archivos

__init__.py

```python

```

__manifest__.py

```python
# -*- coding: utf-8 -*-

{
    'name': "Gestión Hospitalaria",

    'summary': """
        Módulo para la gestión de pacientes, medicos, citas""",

    'description': """
    """,

    'author': "",
    'website': "",

    'category': 'Health',
    'version': '0.1',

    'depends': [
        'hr',
        'sale',
    ],
    'external_dependencies': {
        'python': [
        ]
    },
    'data': [
    ],
    'application': True,
}

```

*Buscar la App creada*
- Click en menú principal `Apps`
- Escribir el nombre de la app `gestion`

Se nota que no se encuentra la app.


*Activar el `modo desarrollador`*

- Click en menú principal `Configuración`
- Click en la parte inferior derecha, en el enlace  `Activar modo desarrollador`


*Actualizar la lista de aplicaciones*

- Click en menú principal `Apps`
- Click en menú lateral `Actualizar lista de aplicaciones`
- Click en botón `Actualizar`


*Instalar la app*

- Click en menú principal `Apps`
- Escribir el nombre de la app `gestion`
- Click en el botón  `Instalar`


[Siguiente](./parte-2.md)
