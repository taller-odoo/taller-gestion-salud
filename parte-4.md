# Relación Médico vs Especialidad

# En modelo médico

En el archivo `models/gestion.py`


```python


class Medico(models.Model):

    especialidad_ids = fields.Many2many(
        comodel_name='gestion.especialidad',
        relation='gestion_medico_especialidad_rel',
        column1='medico_id',
        column2='especialidad_id',
        string='Especialidades')

```

# En la vista formulario del médico

En el archivo `views/gestion_views.xml`

```xml
                        
                        <group string="Especialidades">
                            <field name="especialidad_ids" nolabel="1"/>
                        </group>

```

# Relación Especialidad vs Médico

# En modelo Especialidad

En el archivo `models/gestion.py`


```python


class Especialidad(models.Model):

    medico_ids = fields.Many2many(
        comodel_name='gestion.medico',
        relation='gestion_medico_especialidad_rel',
        column1='especialidad_id',
        column2='medico_id',
        string='Medicos de la especialidad')

```


# En la vista formulario de especialidad

En el archivo `views/gestion_views.xml`

```xml
                        
                        <group string="Médicos">
                            <field name="medico_ids" nolabel="1"/>
                        </group>

```

[Anterior](./parte-3.md) [Siguiente](./parte-4.md)
