# Cita Médica

En el archivo `models/gestion.py`


```python


class Cita(models.Model):
    _name = 'gestion.cita'
    _description = u'Cita médica'

    fecha = fields.Date(
        string='Fecha de la Cita',
        default=fields.Datetime.now,
        required=True,
    )
    especialidad_id = fields.Many2one(
        comodel_name='gestion.especialidad',
        string='Especialidad',
        required=True,
    )
    medico_id = fields.Many2one(
        comodel_name='gestion.medico',
        string='Médico',
        required=True,
    )
    paciente_id = fields.Many2one(
        comodel_name='gestion.paciente',
        string='Paciente',
        required=True,
    )
```

En el archivo `views/gestion_views.xml`

```xml
                        
        <!-- VISTAS DEL MODELO CITA -->

        <!-- VISTA SEARCH -->
        <record id="cita_search_view" model="ir.ui.view">
            <field name="name">gestion.cita.search</field>
            <field name="model">gestion.cita</field>
            <field name="arch" type="xml">
                <search>
                    <field name="fecha"/>
                    <field name="especialidad_id"/>
                    <field name="medico_id"/>
                    <field name="paciente_id"/>
                </search>
            </field>
        </record>

        <!-- VISTA TREE -->
        <record model="ir.ui.view" id="cita_tree_view">
            <field name="name">gestion.cita.tree</field>
            <field name="model">gestion.cita</field>
            <field name="arch" type="xml">
                <tree>
                    <field name="fecha"/>
                    <field name="especialidad_id"/>
                    <field name="medico_id"/>
                    <field name="paciente_id"/>
                </tree>
            </field>
        </record>

        <!-- VISTA FORMULARIO -->
        <record model="ir.ui.view" id="cita_form_view">
            <field name="name">gestion.cita.form</field>
            <field name="model">gestion.cita</field>
            <field name="arch" type="xml">
                <form>
                    <sheet>
                        <group colspan="4">
                            <field name="fecha"/>
                            <field name="especialidad_id"/>
                            <field name="medico_id"/>
                            <field name="paciente_id"/>
                        </group>
                    </sheet>
                </form>
            </field>
        </record>

        <!-- ACTION VIEW -->
        <record id="cita_action" model="ir.actions.act_window">
            <field name="name">Citas Médicas</field>
            <field name="type">ir.actions.act_window</field>
            <field name="res_model">gestion.cita</field>
            <field name="view_mode">tree,form</field>
            <field name="view_type">form</field>
            <field name="limit">25</field>
            <field name="domain">[]</field>
            <field name="context">{}</field>
        </record>



        <!-- MENU CITAS -->
        <menuitem id="menu_citas"
            parent="menu_main"
            action="cita_action"
            sequence="4" />



```


# Filtro medico según especialidad seleccionada

 En el archivo `views/gestion_views.xml`

```xml
        <!-- VISTAS DEL MODELO CITA -->

        <!-- VISTA FORMULARIO -->


                            <field name="medico_id"
                                domain="[('especialidad_ids', 'in', especialidad_id)]"/>
                            <field name="paciente_id"/>


```


[Anterior](./parte-4.md) [Siguiente](./parte-5.md)
